import pandas as pd

excel_file = "data.xlsx"

xls = pd.ExcelFile(excel_file)

sheet_names = xls.sheet_names

dataframes = {}

for sheet_name in sheet_names:
    df = pd.read_excel(excel_file, sheet_name=sheet_name)
    print(df)
    dataframes[sheet_name] = df

officers, complaints, traffic_stops, arrests = dataframes.values()

# Create fully merged df
merged_df = officers.merge(complaints, on='OfficerID', how='left')

merged_df = merged_df.merge(traffic_stops, on='OfficerID', how='left')

merged_df = merged_df.merge(arrests, on='OfficerID', how='left')

# Create partially merged datasets
complaints = officers.merge(complaints, on='OfficerID', how='left')

traffic_stops = officers.merge(traffic_stops, on='OfficerID', how='left')

arrests = officers.merge(arrests, on='OfficerID', how='left')

# Normalize column names
arrests.columns = arrests.columns.str.strip().str.replace(' ', '_').str.replace('(', '').str.replace(')', '')

target = {
    'Disorderly Conduct': 'Non-devastating',
    'MS Liquor Violation': 'Non-devastating',
    'DWI 2 Deg': 'Devastating',
    'Probation Violation': 'Non-devastating',
    'Traffic Violation': 'Non-devastating',
    'Obstruct Process': 'Non-devastating',
    'DWI 4 Deg': 'Devastating',
    'Traffic': 'Non-devastating',
    'Cont Subs 5 Possess': 'Non-devastating',
    'Escape Custody': 'Devastating',
    'Failure to Register': 'Non-devastating',
    'FE HRO Violation': 'Non-devastating',
    'Assault 5': 'Devastating',
    'Indecent Exposure': 'Non-devastating',
    'False Name': 'Non-devastating',
    'Drug Paraphernalia': 'Non-devastating',
    'Property Damage': 'Devastating',
    'Stolen Property': 'Devastating',
    'Shoplifting': 'Non-devastating',
    'Assault 2': 'Devastating',
    'MS DANCO Violation': 'Non-devastating',
    'Simple Robbery': 'Non-devastating',
    'Cont Subs Other': 'Non-devastating',
    'Drugs Sale': 'Non-devastating',
    'Violate OFP': 'Non-devastating',
    'Firing Weapon': 'Devastating',
    'Contempt of Court': 'Non-devastating',
    'OFP Violation': 'Non-devastating',
    'Theft': 'Non-devastating',
    'Assault': 'Devastating',
    'Theft from Building': 'Devastating',
    'RO Violation': 'Non-devastating',
    'Trespass': 'Devastating',
    'Traffric Violation': 'Non-devastating',
    'CC Fraud': 'Non-devastating',
    'Vandalism': 'Devastating',
    'Theft Other': 'Devastating',
    'Forgery': 'Devastating',
    'Theft of Vehicle': 'Devastating',
    'Swindle/Trick': 'Devastating',
    'Burglary 2': 'Devastating',
    'False Representation': 'Non-devastating',
    'Fraud': 'Devastating',
    'Possess Stolen Prop': 'Non-devastating',
    'MS Property Crime': 'Devastating',
    'Possess Ammunition': 'Devastating',
    'Burglary 3': 'Devastating',
    'Counterfeiting': 'Devastating',
    'Damage to Property': 'Devastating',
    'DWI Penalty': 'Non-devastating',
    'Theft from Yards': 'Non-devastating',
    'GM OFP Violation': 'Non-devastating',
    'HRO Violation': 'Non-devastating',
    'MS Possess MJ': 'Non-devastating',
    'Solicit Child': 'Devastating',
    'Cont Subs 4 Possess': 'Devastating',
    'DWI 1 Deg': 'Non-devastating',
    'CSC 1': 'Non-devastating',
    'CSC 4': 'Non-devastating',
    'FE Stalking': 'Non-devastating',
    'Domestic Assault': 'Devastating',
    'False Imprisonment': 'Non-devastating',
    'Weapon Offense': 'Non-devastating',
    'Interfere 911': 'Non-devastating',
    'Liquor Violation': 'Non-devastating',
    'Murder 2': 'Devastating',
    'Terroristic Threats': 'Devastating',
    'Carry Weapon': 'Devastating',
    'CSC 5': 'Devastating',
    'CSC 2': 'Non-devastating',
    'CSC 3': 'Non-devastating',
    'Stalking': 'Devastating',
    'Domestic Abuse': 'Devastating',
    'GM DANCO Violation': 'Non-devastating',
    'MS Possess Liquor': 'Non-devastating',
    'GM Riot': 'Non-devastating',
    'DWI 3 Deg': 'Non-devastating',
    'DWI': 'Non-devastating',
    'Aggravated Robbery': 'Devastating',
    'Contraband': 'Non-devastating',
    'Juvenile Curfew': 'Non-devastating',
    'MS Public Order': 'Non-devastating',
    'Aggravated Assault': 'Devastating',
    'Evade Tax': 'Non-devastating',
    'Cont Subs 4 Sale': 'Non-devastating',
    'MS Public Nuisance': 'Non-devastating',
    'GM Assault': 'Devastating',
    'GM Liquor Violation': 'Non-devastating',
    'Liquor Violations': 'Non-devastating',
    'Possess Weapon': 'Devastating',
    'Drugs/Narcotics': 'Devastating',
    'DL Violations': 'Non-devastating',
    'FE DANCO Violation': 'Non-devastating',
    'Drugs Possess': 'Non-devastating',
    'FE Burglary Tool': 'Devastating',
    'Shoplifting Gear': 'Non-devastating',
    'Cont Subs 3 Possess': 'Non-devastating',
    'Intimidation': 'Devastating',
    'Simple Assault': 'Devastating',
    'FE OFP Violation': 'Devastating',
    'Burglary 1': 'Devastating',
    'Riot': 'Non-devastating',
    'Robbery': 'Devastating',
    'CVO': 'Non-devastating',
    'Cont Subs 2 Possess': 'Non-devastating',
    'Family Offense': 'Devastating',
    'Theft MV': 'Non-devastating',
    'Theft from MV': 'Non-devastating',
    'GM HRO Violation': 'Non-devastating',
    'Obscenities': 'Devastating',
    'Ordinance': 'Non-devastating',
    'Theft from Mail': 'Non-devastating',
    'Cont Subs 3 Sale': 'Non-devastating',
    'Cont Subs 2 Sale': 'Non-devastating',
    'Tobacco': 'Non-devastating',
    'Mail Theft': 'Non-devastating',
    'False Report': 'Non-devastating',
    'MS Other Harassment': 'Non-devastating',
    'Emergency 911': 'Non-devastating',
    'Assault 4': 'Devastating',
    'MS Harassing Phone C': 'Non-devastating',
    'Prostitution': 'Non-devastating',
    'Cont Subs 1 Sale': 'Non-devastating',
    'Cont Subs Possess': 'Non-devastating',
    'Racketeering': 'Non-devastating',
    'Cont Subs 5 Sale': 'Non-devastating',
    'Meth Crimes': 'Devastating',
    'Possess Stolen MV': 'Non-devastating',
    'Cont Subs 1 Int Sell': 'Non-devastating',
    'Cont Subs 1 Possess': 'Non-devastating',
    'DANCO': 'Non-devastating',
    'Cont Subs 5 Procure': 'Non-devastating',
    'Harm Police Dog': 'Devastating',
    'Assault 3': 'Devastating',
    'Solicitation': 'Non-devastating'
}

arrests['target'] = arrests.SubjectCrimeType.map(target)

# Transform object variables into categorical
categorical_vars = ['OfficerGender', 'OfficerRace', 'OfficerAge', 'OfficerRank', 'SubjectRace', 'SubjectCrimeType', 'Type', 'target', 'OfficerMilitaryExperience']
arrests[categorical_vars] = arrests[categorical_vars].astype('category')

# Transform object variables into numerical
numerical_vars = ['OfficerMilitaryExperience']
arrests[numerical_vars] = arrests[numerical_vars].astype(float)

# Transform object variables into datetime
date_vars = ['EmploymentStartDate', 'Date']
arrests[date_vars] = arrests[date_vars].apply(pd.to_datetime)

# Plot histograms of numeric variables
arrests.hist(figsize=(10, 6))

# Count the occurrences of each category in categorical variables
categorical_vars = ['OfficerGender', 'OfficerRace', 'OfficerAge', 'OfficerRank', 'SubjectRace', 'SubjectCrimeType', 'target']
for var in categorical_vars:
    print(arrests[var].value_counts())

# Drop rows with missing target values
arrests = arrests.dropna(subset=['target'])

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import classification_report
from sklearn.linear_model import LogisticRegression

# Encode categorical variables
le = LabelEncoder()
for var in ['OfficerAge', 'OfficerRank', 'OfficerMilitaryExperience']:
    arrests[var] = le.fit_transform(arrests[var])

# Encode variables
arrests = pd.get_dummies(arrests, columns=['OfficerGender', 'OfficerRace', 'SubjectRace'])

X = ['OfficerAge', 'OfficerMilitaryExperience', 'OfficerRank', 'OfficerGender_Female',
       'OfficerGender_Male', 'OfficerRace_Asian',
       'OfficerRace_Black or African American',
       'OfficerRace_Hispanic or Latino', 'OfficerRace_White',
       'SubjectRace_American Indian/Alaskan Native',
       'SubjectRace_American Indian_Alaskan Native Hispanic',
       'SubjectRace_Asian Hispanic', 'SubjectRace_Asian_Pacific Islander',
       'SubjectRace_Black', 'SubjectRace_Black Hispanic',
       'SubjectRace_Hawaiian/Pacific Islander', 'SubjectRace_Hispanic',
       'SubjectRace_Unknown', 'SubjectRace_White']
y = ['target']

# Split the data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(arrests[X], arrests[y], test_size=0.2, random_state=42)

# Create a logistic regression classifier, base model
logreg = LogisticRegression()

# Train the model
logreg.fit(X_train, y_train)

# Make predictions on the test set
y_pred_logreg = logreg.predict(X_test)

# Evaluate the model
report_logreg = classification_report(y_test, y_pred_logreg)
print(report_logreg)


# Create a decision tree classifier
clf = DecisionTreeClassifier()

# Train the model
clf.fit(X_train, y_train)

# Make predictions on the test set
y_pred = clf.predict(X_test)

# Evaluate the model
report = classification_report(y_test, y_pred)
print(report)

# Explore feature importance. 
importances = clf.feature_importances_

# Pair the feature names with their importances
feature_importances = zip(X, importances)

# Sort the features by their importances in descending order
sorted_importances = sorted(feature_importances, key=lambda x: x[1], reverse=True)

# Print the feature importances
for feature, importance in sorted_importances:
    print(f"{feature}: {importance}")


# Try balancing the data
from imblearn.over_sampling import RandomOverSampler

# Create an instance of RandomOverSampler
ros = RandomOverSampler(random_state=42)

# Resample the dataset
X_resampled, y_resampled = ros.fit_resample(arrests[X], arrests[y])

# Split the resampled data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X_resampled, y_resampled, test_size=0.2, random_state=42)

# Train and evaluate the model with the balanced dataset
clf2 = DecisionTreeClassifier()
clf2.fit(X_train, y_train)
y_pred = clf2.predict(X_test)
report = classification_report(y_test, y_pred)
print(report)

# Explore feature importance. 
importances = clf2.feature_importances_

# Pair the feature names with their importances
feature_importances = zip(X, importances)

# Sort the features by their importances in descending order
sorted_importances = sorted(feature_importances, key=lambda x: x[1], reverse=True)

# Print the feature importances
for feature, importance in sorted_importances:
    print(f"{feature}: {importance}")

# Create a mock input value for the model
mock_input = {'OfficerAge': 30, 'OfficerMilitaryExperience': 1, 'OfficerRank': 1,
              'OfficerGender_Female': 0, 'OfficerGender_Male': 1, 'OfficerRace_Asian': 0,
              'OfficerRace_Black or African American': 0, 'OfficerRace_Hispanic or Latino': 0,
              'OfficerRace_White': 1, 'SubjectRace_American Indian/Alaskan Native': 0,
              'SubjectRace_American Indian_Alaskan Native Hispanic': 0,
              'SubjectRace_Asian Hispanic': 0, 'SubjectRace_Asian_Pacific Islander': 0,
              'SubjectRace_Black': 1, 'SubjectRace_Black Hispanic': 0,
              'SubjectRace_Hawaiian/Pacific Islander': 0, 'SubjectRace_Hispanic': 0,
              'SubjectRace_Unknown': 0, 'SubjectRace_White': 0}

# Feed the mock input to the model
mock_input_df = pd.DataFrame(mock_input, index=[0])
clf2.predict(mock_input_df)